package com.zencode.crmrest.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class CustomerAspectLogger {
    private static final Logger logger = Logger.getLogger(CustomerAspectLogger.class.getName());
    private final Environment env;

    @Autowired
    public CustomerAspectLogger(Environment env) {
        this.env = env;
    }

    @Before("com.zencode.crmrest.aop.expression.Expressions.forAppDataSource()")
    public void beforeDataSource() {
        logger.info(">>>> @beforeDataSource JDBC URL=" + env.getProperty("jdbc.url"));
        logger.info(">>>> @beforeDataSource JDBC User=" + env.getProperty("jdbc.user"));
    }

    @AfterThrowing(
            pointcut = "com.zencode.crmrest.aop.expression.Expressions.forApplication()",
            throwing = "exception"
    )
    public void afterThrowing(JoinPoint joinPoint, Throwable exception) {
        logger.info(">>>> @AfterThrowing on method: " + joinPoint.getSignature());
        logger.info(">>>> Exception: " + exception);
    }
}
