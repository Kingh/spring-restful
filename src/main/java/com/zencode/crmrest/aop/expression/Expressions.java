package com.zencode.crmrest.aop.expression;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Expressions {
    @Pointcut("execution(* com.zencode.crmrest.config.CRMApplicationConfig.appDataSource())")
    public void forAppDataSource() {}

    @Pointcut("execution(* com.zencode.crmrest.*.*.*(..))")
    public void forApplication() {}
}
