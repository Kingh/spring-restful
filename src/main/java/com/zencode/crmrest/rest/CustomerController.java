package com.zencode.crmrest.rest;

import com.zencode.crmrest.entity.Customer;
import com.zencode.crmrest.exception.CustomerNotFoundException;
import com.zencode.crmrest.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerController {
    private final ICustomerService service;

    @Autowired
    public CustomerController(ICustomerService service) {
        this.service = service;
    }

    @GetMapping("/customers/{customerId}")
    public Customer getCustomer(@PathVariable int customerId) {
        Customer customer = service.getCustomer(customerId);
        if (customer == null) {
            throw new CustomerNotFoundException("Customer id not found - " + customerId);
        }

        return customer;
    }

    @GetMapping("/customers")
    public List<Customer> getCustomers() {
        return service.getCustomers();
    }

    @PostMapping("/customers")
    public Customer addCustomer(@Valid @RequestBody Customer customer) {
        customer.setId(0);

        service.saveOrUpdate(customer);

        return customer;
    }

    @PutMapping("/customers")
    public Customer updateCustomer(@Valid @RequestBody Customer customer) {
        service.saveOrUpdate(customer);

        return customer;
    }

    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<String> deleteCustomer(@PathVariable int customerId) {
        Customer customer = service.getCustomer(customerId);
        if (customer == null) {
            throw new CustomerNotFoundException("Customer id not found - " + customerId);
        }

        service.deleteCustomer(customerId);

        return ResponseEntity.ok("Deleted customer id - " + customerId);
    }

    @InitBinder
    public void trim(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
}
