package com.zencode.crmrest.dao;

import com.zencode.crmrest.entity.Customer;

import java.util.List;

public interface ICustomerDAO {
    void saveOrUpdate(Customer customer);
    List<Customer> getCustomers();
    Customer getCustomer(int customerId);
    void deleteCustomer(int customerId);
}
