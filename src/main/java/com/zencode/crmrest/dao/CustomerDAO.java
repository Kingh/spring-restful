package com.zencode.crmrest.dao;

import com.zencode.crmrest.entity.Customer;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDAO implements ICustomerDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveOrUpdate(Customer customer) {
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(customer);
    }

    @Override
    public List<Customer> getCustomers() {
        return sessionFactory
                .getCurrentSession()
                .createQuery("FROM Customer", Customer.class)
                .getResultList();
    }

    @Override
    public Customer getCustomer(int customerId) {
        return sessionFactory
                .getCurrentSession()
                .get(Customer.class, customerId);
    }

    @Override
    public void deleteCustomer(int customerId) {
        Customer customer = sessionFactory.getCurrentSession().get(Customer.class, customerId);

        sessionFactory
                .getCurrentSession()
                .delete(customer);
    }
}
