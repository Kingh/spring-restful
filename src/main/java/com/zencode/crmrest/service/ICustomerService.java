package com.zencode.crmrest.service;

import com.zencode.crmrest.entity.Customer;

import java.util.List;

public interface ICustomerService {
    void saveOrUpdate(Customer customer);
    List<Customer> getCustomers();
    Customer getCustomer(int customerId);
    void deleteCustomer(int customerId);
}
