package com.zencode.crmrest.service;

import com.zencode.crmrest.dao.ICustomerDAO;
import com.zencode.crmrest.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustomerService implements ICustomerService {
    private final ICustomerDAO dao;

    @Autowired
    public CustomerService(ICustomerDAO dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public void saveOrUpdate(Customer customer) {
        dao.saveOrUpdate(customer);
    }

    @Override
    @Transactional
    public List<Customer> getCustomers() {
        return dao.getCustomers();
    }

    @Override
    @Transactional
    public Customer getCustomer(int customerId) {
        return dao.getCustomer(customerId);
    }

    @Override
    @Transactional
    public void deleteCustomer(int customerId) {
        dao.deleteCustomer(customerId);
    }
}
