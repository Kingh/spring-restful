package com.zencode.crmrest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Pattern(
        regexp = Constants.PATTERN,
        flags = {Flag.CASE_INSENSITIVE},
        message = "Bad email format"
)
public @interface Email {
    String message() default "Bad email format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}